package controller;

import model.Baralho;
import model.Mao;

public class Jogo extends Thread {
	private Mao jogadorUm;
	private Mao jogadorDois;
	private Baralho baralho;

	public void run() {
		try {
			baralho = new Baralho();
			jogadorUm = new Mao(baralho.obterCartasDoJogador());
			jogadorDois = new Mao(baralho.obterCartasDoJogador());
			
			System.out.println("Jogador um: " + jogadorUm.toString());
			System.out.println("Jogador dois: " + jogadorDois.toString());
			compararResultado();
			
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Baralho viciado. Adquira outro baralho!\n" +e.getMessage());
		}
	}

	
	private void compararResultado() {
		if (jogadorUm.getMao() > jogadorDois.getMao()) {
			exibirResultado(jogadorUm);
		} else if (jogadorUm.getMao() < jogadorDois.getMao()) {
			exibirResultado(jogadorDois);
		} else {
			if (jogadorUm.getCartaChave().getValor() > jogadorDois.getCartaChave().getValor()) {
				exibirResultado(jogadorUm);
			} else if (jogadorUm.getCartaChave().getValor() < jogadorDois.getCartaChave().getValor()) {
				exibirResultado(jogadorDois);
			} else {
				for (int i = 0; i < jogadorUm.getKickers().size(); i++) {
					if (jogadorUm.getKickers().get(i).getValor() > jogadorDois.getKickers().get(i).getValor()) {
						exibirResultado(jogadorUm);
						break;
					} else if (jogadorUm.getKickers().get(i).getValor() < jogadorDois.getKickers().get(i).getValor()) {
						exibirResultado(jogadorDois);
						break;
					}
				}
			}

		}

	}

	private void exibirResultado(Mao jogadorVencedor) {
		System.out.println("\nM�o vencedora:");
		System.out.println(jogadorVencedor.toString());
	}
}
