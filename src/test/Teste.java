package test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import model.Carta;
import model.Mao;

public class Teste extends TestCase {
	
	public void testValorMao() {
		List<Carta> cartasTesteUm = new ArrayList<Carta>();
		cartasTesteUm.add(new Carta(1, Carta.SPADES));
		cartasTesteUm.add(new Carta(2, Carta.CLUBS));
		cartasTesteUm.add(new Carta(3, Carta.DIAMONDS));
		cartasTesteUm.add(new Carta(4, Carta.HEARTS));
		cartasTesteUm.add(new Carta(5, Carta.SPADES));
		
		Mao testeMao = new Mao(cartasTesteUm);
		assertEquals(testeMao.getMao(), Mao.SEQUENCIA);
		
		List<Carta> cartasTesteDois = new ArrayList<Carta>();
		cartasTesteDois.add(new Carta(1, Carta.SPADES));
		cartasTesteDois.add(new Carta(1, Carta.CLUBS));
		cartasTesteDois.add(new Carta(1, Carta.DIAMONDS));
		cartasTesteDois.add(new Carta(1, Carta.HEARTS));
		cartasTesteDois.add(new Carta(5, Carta.SPADES));
		
		Mao testeMaoDois = new Mao(cartasTesteDois);
		assertEquals(testeMaoDois.getMao(), Mao.QUADRA);
		
		List<Carta> cartasTesteTres = new ArrayList<Carta>();
		cartasTesteTres.add(new Carta(1, Carta.SPADES));
		cartasTesteTres.add(new Carta(1, Carta.CLUBS));
		cartasTesteTres.add(new Carta(2, Carta.DIAMONDS));
		cartasTesteTres.add(new Carta(3, Carta.HEARTS));
		cartasTesteTres.add(new Carta(5, Carta.SPADES));
		
		Mao testeMaoTres = new Mao(cartasTesteTres);
		assertEquals(testeMaoTres.getMao(), Mao.PAR);
		
		List<Carta> cartasTesteQuatro = new ArrayList<Carta>();
		cartasTesteQuatro.add(new Carta(1, Carta.SPADES));
		cartasTesteQuatro.add(new Carta(1, Carta.SPADES));
		cartasTesteQuatro.add(new Carta(2, Carta.SPADES));
		cartasTesteQuatro.add(new Carta(3, Carta.SPADES));
		cartasTesteQuatro.add(new Carta(5, Carta.SPADES));
		
		Mao testeMaoQuatro = new Mao(cartasTesteQuatro);
		assertEquals(testeMaoQuatro.getMao(), Mao.FLUSH);
	}
	
	public void testVencedor(){
		List<Carta> cartasTesteUm = new ArrayList<Carta>();
		cartasTesteUm.add(new Carta(1, Carta.SPADES));
		cartasTesteUm.add(new Carta(2, Carta.SPADES));
		cartasTesteUm.add(new Carta(3, Carta.SPADES));
		cartasTesteUm.add(new Carta(4, Carta.SPADES));
		cartasTesteUm.add(new Carta(5, Carta.SPADES));
		
		Mao maoUmTesteUm = new Mao(cartasTesteUm);
		
		List<Carta> cartasDoisTesteUm = new ArrayList<Carta>();
		cartasDoisTesteUm.add(new Carta(1, Carta.SPADES));
		cartasDoisTesteUm.add(new Carta(1, Carta.CLUBS));
		cartasDoisTesteUm.add(new Carta(1, Carta.DIAMONDS));
		cartasDoisTesteUm.add(new Carta(1, Carta.HEARTS));
		cartasDoisTesteUm.add(new Carta(5, Carta.SPADES));
		
		Mao maoDoisTesteUm = new Mao(cartasDoisTesteUm);
		
		assertEquals(maoUmTesteUm.getMao() > maoDoisTesteUm.getMao(), true);
		
		List<Carta> cartasTesteDois = new ArrayList<Carta>();
		cartasTesteDois.add(new Carta(1, Carta.SPADES));
		cartasTesteDois.add(new Carta(2, Carta.SPADES));
		cartasTesteDois.add(new Carta(3, Carta.SPADES));
		cartasTesteDois.add(new Carta(4, Carta.SPADES));
		cartasTesteDois.add(new Carta(5, Carta.SPADES));
		
		Mao maoUmTesteDois = new Mao(cartasTesteDois);
		
		List<Carta> cartasDoisTesteDois = new ArrayList<Carta>();
		cartasDoisTesteDois.add(new Carta(1, Carta.SPADES));
		cartasDoisTesteDois.add(new Carta(1, Carta.CLUBS));
		cartasDoisTesteDois.add(new Carta(1, Carta.DIAMONDS));
		cartasDoisTesteDois.add(new Carta(1, Carta.HEARTS));
		cartasDoisTesteDois.add(new Carta(5, Carta.SPADES));
		
		Mao maoDoisTesteDois = new Mao(cartasDoisTesteDois);
		
		assertEquals(maoUmTesteDois.getMao() > maoDoisTesteDois.getMao(), true);
		
	}
	
}
