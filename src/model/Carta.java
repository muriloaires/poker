package model;

public class Carta {

	/** naipes **/
	public static final int DIAMONDS = -1;
	public static final int HEARTS = -2;
	public static final int SPADES = -3;
	public static final int CLUBS = -4;

	/** valores **/
	public static final int DOIS = 2;
	public static final int TRES = 3;
	public static final int QUATRO = 4;
	public static final int CINCO = 5;
	public static final int SEIS = 6;
	public static final int SETE = 7;
	public static final int OITO = 8;
	public static final int NOVE = 9;
	public static final int DEZ = 10;
	public static final int VALETE = 11;
	public static final int DAMA = 12;
	public static final int REI = 13;
	public static final int AS = 14;

	private int valor;
	private int naipe;

	public Carta(int valor, int naipe) {
		this.valor = valor;
		this.naipe = naipe;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getNaipe() {
		return naipe;
	}

	public void setNaipe(int naipe) {
		this.naipe = naipe;
	}

	

	private String getNomeNaipe() {
		switch (naipe) {
		case DIAMONDS:
			return "D";
		case HEARTS:
			return "H";
		case SPADES:
			return "S";
		case CLUBS:
			return "C";
		default:
			return "";
		}
	}

	private String getNomeCarta() {
		switch (valor) {
		case DOIS:
			return "2";
		case TRES:
			return "3";
		case QUATRO:
			return "4";
		case CINCO:
			return "5";
		case SEIS:
			return "6";
		case SETE:
			return "7";
		case OITO:
			return "8";
		case NOVE:
			return "9";
		case DEZ:
			return "10";
		case VALETE:
			return "J";
		case DAMA:
			return "Q";
		case REI:
			return "K";
		case AS:
			return "A";
		default:
			return "";
		}
	}
	
	@Override
	public String toString() {
		return getNomeCarta() + getNomeNaipe();
	}
}
