package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Baralho {
	private List<Carta> cartas;
	private int cartasDisponiveis;

	public Baralho() {
		cartas = new ArrayList<Carta>();
		cartasDisponiveis = 52;
		recarregarBaralho();
	}

	public void recarregarBaralho() {

		for (int i = Carta.DIAMONDS; i >= Carta.CLUBS; i--) {
			for (int j = 2; j < 15; j++) {
				Carta carta = new Carta(j, i);
				cartas.add(carta);
			}
		}

	}

	public List<Carta> obterCartasDoJogador() throws IndexOutOfBoundsException {
		ArrayList<Carta> retorno = new ArrayList<Carta>();

		Random rd = new Random();
		for (int i = 0; i < 5; i++) {
			int index = rd.nextInt(cartasDisponiveis - 1);
			Carta carta = cartas.get(index);
			retorno.add(carta);
			
			cartas.remove(index);
			cartasDisponiveis--;
		}
		return retorno;
	}
}
