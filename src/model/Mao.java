package model;

import java.util.ArrayList;
import java.util.List;

public class Mao {

	/** Valores possiveis da m�o **/
	public static final int CARTA_ALTA = 1;
	public static final int PAR = 2;
	public static final int DOIS_PARES = 3;
	public static final int TRINCA = 4;
	public static final int SEQUENCIA = 5;
	public static final int FLUSH = 6;
	public static final int FULL_HOUSE = 7;
	public static final int QUADRA = 8;
	public static final int STRAIGHT_FLUSH = 9;
	public static final int ROYAL_FLUSH = 10;

	private List<Carta> cartasDoJogador;
	private List<Carta> kickers;

	public List<Carta> getKickers() {
		return kickers;
	}

	public Mao(List<Carta> cartasDoJogador) {
		this.cartasDoJogador = cartasDoJogador;
		ordernarCartasDaMao();
	}
	
	//usado para debug
	public Mao() {
		cartasDoJogador = new ArrayList<Carta>();
		cartasDoJogador.add(new Carta(6, Carta.SPADES));
		cartasDoJogador.add(new Carta(9, Carta.CLUBS));
		cartasDoJogador.add(new Carta(11, Carta.DIAMONDS));
		cartasDoJogador.add(new Carta(12, Carta.HEARTS));
		cartasDoJogador.add(new Carta(14, Carta.SPADES));
		ordernarCartasDaMao();
	}

	/**
	 * Carta que representa o valor da m�o.
	 */
	private Carta cartaChave;

	public Carta getCartaChave() {
		return cartaChave;
	}

	public List<Carta> getCartasDoJogador() {
		return cartasDoJogador;
	}

	public int getMao() {
		if (isStraight() && isFlush() && (obterMaiorCarta() == Carta.AS)) {
			return ROYAL_FLUSH;
		} else if (isStraight() && isFlush()) {
			return STRAIGHT_FLUSH;
		} else if (isQuadra()) {
			return QUADRA;
		} else if (isFullHouse()) {
			return FULL_HOUSE;
		} else if (isFlush()) {
			return FLUSH;
		} else if (isStraight()) {
			return SEQUENCIA;
		} else if (isTrinca()) {
			return TRINCA;
		} else if (isTwoPair()) {
			return DOIS_PARES;
		} else if (isPar()) {
			return PAR;
		} else {
			cartaChave = cartasDoJogador.get(4);
			kickers = new ArrayList<Carta>();
			kickers.add(cartasDoJogador.get(3));
			kickers.add(cartasDoJogador.get(2));
			kickers.add(cartasDoJogador.get(1));
			kickers.add(cartasDoJogador.get(0));
			return CARTA_ALTA;
		}
	}

	private void ordernarCartasDaMao() {
		Carta maiorCarta = cartasDoJogador.get(0);
		for (int i = 0; i < cartasDoJogador.size(); i++) {
			for (int j = 0; j < cartasDoJogador.size(); j++) {
				if (cartasDoJogador.get(i).getValor() < cartasDoJogador.get(j)
						.getValor()) {
					maiorCarta = cartasDoJogador.get(i);
					cartasDoJogador.set(i, cartasDoJogador.get(j));
					cartasDoJogador.set(j, maiorCarta);
				}
			}
		}
	}

	private boolean isFlush() {
		boolean retorno = true;
		/** Verifica se os naipes das cartas da m�o s�o os mesmos **/
		Carta carta = cartasDoJogador.get(0);
		for (int i = 0; i < cartasDoJogador.size(); i++) {
			if(i == 4){
				break;
			}
			if (!(carta.getNaipe() == cartasDoJogador.get(i + 1).getNaipe())) {
				retorno = false;
				break;
			}
		}

		if (retorno) {
			cartaChave = cartasDoJogador.get(4);
		}

		return retorno;

	}

	private boolean isStraight() {
		boolean isstraight = true;

		if (!((cartasDoJogador.get(0).getValor() + 1 == cartasDoJogador.get(1).getValor() )
				&& (cartasDoJogador.get(0).getValor() + 2 == cartasDoJogador.get(2).getValor() )
				&& (cartasDoJogador.get(0).getValor() + 3 == cartasDoJogador.get(3)	.getValor()) 
				&& (cartasDoJogador.get(0).getValor() + 4 == cartasDoJogador.get(4).getValor()))) {
			isstraight = false;
		}

		if (isstraight) {
			cartaChave = cartasDoJogador.get(4);
		}
		return isstraight;
	}

	private int obterMaiorCarta() {
		return cartasDoJogador.get(4).getValor();
	}

	private boolean isQuadra() {
		boolean primeiraVerifificacao, segundaVerificacao;

		primeiraVerifificacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor()
				&& cartasDoJogador.get(1).getValor() == cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(2).getValor() == cartasDoJogador.get(3).getValor();

		segundaVerificacao = cartasDoJogador.get(1).getValor() == cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(2).getValor() == cartasDoJogador.get(3).getValor()
				&& cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor();
		kickers = new ArrayList<>();

		if (primeiraVerifificacao) {
			cartaChave = cartasDoJogador.get(0);
			kickers.add(cartasDoJogador.get(4));
		} else if (segundaVerificacao) {
			cartaChave = cartasDoJogador.get(4);
			kickers.add(cartasDoJogador.get(0));
		}

		return primeiraVerifificacao || segundaVerificacao;
	}

	private boolean isTrinca() {
		boolean primeiraVerificacao = false, segundaVerificacao = false, terceiraVerificacao = false;

		primeiraVerificacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor()
				&& cartasDoJogador.get(1).getValor() == cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(3).getValor() != cartasDoJogador.get(0).getValor()
				&& cartasDoJogador.get(4).getValor() != cartasDoJogador.get(0).getValor()
				&& cartasDoJogador.get(3).getValor() != cartasDoJogador.get(4).getValor();

		segundaVerificacao = cartasDoJogador.get(1).getValor() == cartasDoJogador
				.get(2).getValor()
				&& cartasDoJogador.get(2).getValor() == cartasDoJogador.get(3).getValor()
				&& cartasDoJogador.get(3).getValor() != cartasDoJogador.get(0).getValor()
				&& cartasDoJogador.get(4).getValor() != cartasDoJogador.get(0).getValor()
				&& cartasDoJogador.get(3).getValor() != cartasDoJogador.get(4).getValor();

		terceiraVerificacao = cartasDoJogador.get(2).getValor() == cartasDoJogador
				.get(3).getValor()
				&& cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor()
				&& cartasDoJogador.get(0).getValor() != cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(1).getValor() != cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(0).getValor() != cartasDoJogador.get(1).getValor();

		kickers = new ArrayList<Carta>();
		if (primeiraVerificacao) {
			cartaChave = cartasDoJogador.get(0);
			kickers.add(cartasDoJogador.get(4));
			kickers.add(cartasDoJogador.get(3));
		} else if (segundaVerificacao) {
			cartaChave = cartasDoJogador.get(1);
			kickers.add(cartasDoJogador.get(4));
			kickers.add(cartasDoJogador.get(1));
		} else if (terceiraVerificacao) {
			cartaChave = cartasDoJogador.get(2);
			kickers.add(cartasDoJogador.get(1));
			kickers.add(cartasDoJogador.get(0));
		}

		return primeiraVerificacao || segundaVerificacao || terceiraVerificacao;

	}

	private boolean isPar() {
		boolean primeiraVerificacao = false, segundaVerificacao = false, terceiraVerificacao = false, quartaVerificacao = false;

		primeiraVerificacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor();
		segundaVerificacao  = cartasDoJogador.get(1).getValor() == cartasDoJogador.get(2).getValor();
		terceiraVerificacao = cartasDoJogador.get(2).getValor() == cartasDoJogador.get(3).getValor();
		quartaVerificacao   = cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor();

		kickers = new ArrayList<Carta>();
		if (primeiraVerificacao) {
			cartaChave = cartasDoJogador.get(0);
			kickers.add(cartasDoJogador.get(4));
			kickers.add(cartasDoJogador.get(3));
			kickers.add(cartasDoJogador.get(2));
		} else if (segundaVerificacao) {
			cartaChave = cartasDoJogador.get(1);
			kickers.add(cartasDoJogador.get(4));
			kickers.add(cartasDoJogador.get(3));
			kickers.add(cartasDoJogador.get(0));
		} else if (terceiraVerificacao) {
			cartaChave = cartasDoJogador.get(2);
			kickers.add(cartasDoJogador.get(4));
			kickers.add(cartasDoJogador.get(1));
			kickers.add(cartasDoJogador.get(0));
		} else if (quartaVerificacao) {
			cartaChave = cartasDoJogador.get(3);
			kickers.add(cartasDoJogador.get(2));
			kickers.add(cartasDoJogador.get(1));
			kickers.add(cartasDoJogador.get(0));
		}

		return primeiraVerificacao || segundaVerificacao || terceiraVerificacao
				|| quartaVerificacao;
	}

	private boolean isFullHouse() {
		boolean primeiraVerficacao = false, segundaVerificacao = false;

		primeiraVerficacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor()
				&& cartasDoJogador.get(1).getValor() == cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor();

		segundaVerificacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor()
				&& cartasDoJogador.get(2).getValor() == cartasDoJogador.get(3).getValor()
				&& cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor();

		if (primeiraVerficacao) {
			cartaChave = cartasDoJogador.get(0);
		} else if (segundaVerificacao) {
			cartaChave = cartasDoJogador.get(2);
		}
		return primeiraVerficacao || segundaVerificacao;
	}

	private boolean isTwoPair() {

		boolean primeiraVerificacao = false, segundaVerificacao = false, terceiraVerificacao = false;
		primeiraVerificacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor()
				&& cartasDoJogador.get(2).getValor() == cartasDoJogador.get(3).getValor();

		segundaVerificacao = cartasDoJogador.get(0).getValor() == cartasDoJogador.get(1).getValor()
				&& cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor();

		terceiraVerificacao = cartasDoJogador.get(1).getValor() == cartasDoJogador.get(2).getValor()
				&& cartasDoJogador.get(3).getValor() == cartasDoJogador.get(4).getValor();

		kickers = new ArrayList<Carta>();
		if (primeiraVerificacao) {
			cartaChave = cartasDoJogador.get(2);
			kickers.add(cartasDoJogador.get(4));
		} else if (segundaVerificacao) {
			cartaChave = cartasDoJogador.get(3);
			kickers.add(cartasDoJogador.get(2));
		} else if (terceiraVerificacao) {
			cartaChave = cartasDoJogador.get(3);
			kickers.add(cartasDoJogador.get(0));
		}

		return primeiraVerificacao || segundaVerificacao || terceiraVerificacao;
	}

	@Override
	public String toString() {
		String retorno = "";
		for (Carta carta : cartasDoJogador) {
			retorno += carta.toString() + " ";
		}
		retorno += " --- ";
		switch (getMao()) {
		
		case ROYAL_FLUSH:
			retorno += "*** EPIC *** ROYAL FLUSH";
			break;
		case STRAIGHT_FLUSH:
			retorno += "STRAIGHT FLUSH";
			break;
		case QUADRA:
			retorno += "FOUR OF A KIND";
			break;
		case FULL_HOUSE:
			retorno += "FULL HOUSE";
			break;
		case FLUSH:
			retorno += "FLUSH";
			break;
		case SEQUENCIA:
			retorno += "STRAIGHT";
			break;
		case TRINCA:
			retorno += "THREE OF A KIND";
			break;
		case DOIS_PARES:
			retorno += "TWO PAIR";
			break;
		case PAR:
			retorno += "PAIR";
			break;
		case CARTA_ALTA:
			retorno += "HIGH CARD";
			break;
		
		}
		
		return retorno;
	}
}
